Original repository: https://github.com/Urigo/meteor-rxjs

This is a fork of meteor-rxjs that incorporates all important pull requests till August 2020.
Although Meteor is a popular backend solution and Angular is one of the most used frameworks
the meteor-rxjs repository hasn't been updated in over 4 years.

## List of changes:

- \#247 Stop relying on underscore.js' _ being magically available 
- \#245 Regression test and fix for ObservableCursor aliasing bug
- \#186 Fix problem with using in "optimistic ui" mode

Also added minor changes for better support of Angular up to version 10.
